<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            function showTotal() {
                var bPrice = (document.f.buyingPrice.value * 100) / 100;
                var quantity = (document.f.productCount.value * 100) / 100;
                document.f.total.value = bPrice * quantity;
            }

        </script>
        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div class="row">
                <c:forEach items="${cart}" var="cart">
                    <div class="col-md-4">              
                        <div>
                            <img src="static-resource/images/${cart.product.code}.jpg" alt="" style="height: 150px; width: 150px"/><br/>
                        </div>
                        <form action="/E-Shop/editCart.idb" method="post" name="f">
                            <div>
                                <table style="font-size: 15px;">
                                    <tr class="hidden">
                                        <td><b>Cart ID</b></td>
                                        <td><input type="text" value="${cart.id}" style="border: 0" name="id" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td><b>ID</b></td>
                                        <td><input type="text" value="${cart.product.id}" style="border: 0" name="product.id" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td><b>Name</b></td>
                                        <td>${cart.product.name}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Brand</b></td>
                                        <td>${cart.product.brand}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Description </b></td>
                                        <td>${cart.product.description}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Buying Price</b></td>
                                        <td><input type="text" value="${cart.product.unitPrice}" style="border: 0" name="buyingPrice" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td><b>Quantity</b></td>
                                        <td><input type="text" value="${cart.productCount}" style="border: 0" name="productCount" onkeyup="showTotal()"/></td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Price</b></td>
                                        <td><input type="text" value="${cart.product.unitPrice}" style="border: 0" name="total" readonly/></td>
                                    </tr>
                                </table>
                            </div> 
                            <div>
                                <input type="submit" value="Edit" class="btn btn-primary"/> &nbsp;
                                <a class="btn btn-danger" href="/E-Shop/deleteCart.idb?id=${cart.id}">Delete</a>&nbsp;                                
                            </div>
                        </form>
                    </div>
                    <br/>
                </c:forEach>
            </div>
            <br/>
            <a class="btn btn-info pull-left" href="/E-Shop/checkout.idb">Check Out</a>
            <br/>
        </div>

    </body>
</html>
