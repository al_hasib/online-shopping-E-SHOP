<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <%@include file="shared/sidebar.jsp" %>
        </div>
        <div class="col-md-9"><br/>
            <div class="row">
                <div class="col-lg-12">
                    <c:if test="${clickAllProducts == true}">
                        <script>
                            window.categoryId = '';
                        </script>
                        <ol class="breadcrumb ">
                            <li><a href="${contextRoot}/index.idb">Home</a></li>
                            <li class="active"> / All Products</li>
                        </ol>
                    </c:if>
                    <c:if test="${clickCategoryProduct == true}">
                        <script>
                            window.categoryId = '${category.id}';
                        </script>
                        <ol class="breadcrumb ">
                            <li><a href="${contextRoot}/index.idb">Home </a></li>
                            <li class="active"> / Category /</li>
                            <li class="active"> ${category.name}</li>
                        </ol>
                        <!-- Modal -->
                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="singleProductDetail">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Product Detail :</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <img src="${images}/${product.code}.jpg" class="mImage"/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="id">Product ID : ${product.id}</label><br>
                                                        <label for="name">Product Name : ${product.name}</label><br>
                                                        <label for="price">Price : ${product.unitPrice}</label><br>
                                                        <label for="qnt">Product Quantity : ${product.quantity}</label><br>
                                                        Product Description :<br>
                                                        <textarea rows="6" cols="50" readonly> ${product.description}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </c:if> 

                    </div>
                </div>
                
                <div class="col-xs-12 col-md-12">
                    <table id="productListTable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Brand</th>
                                <th>Price</th>
                                <th>Qut. Available</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


