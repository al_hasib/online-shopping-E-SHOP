<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>

        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>User ID</th>
                            <th>Total Price</th>
                            <th>Total Item</th>
                            <th>Order Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${order}" var="order"> 
                            <tr>
                                <td><a href="/E-Shop/product/showOrderItem.idb?id=${order.id}">${order.id}</a></td>
                                <td><a href="/E-Shop/product/showUser.idb?id=${order.userId}">${order.userId}</a></td>
                                <td>${order.orderTotal}</td>
                                <td>${order.orderCount}</td>
                                <td>${order.orderDate}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
