<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('${images}/img_1.png')">
        <div class="carousel-caption d-none d-md-block">
          <p class="lead">This is a description for the first slide.</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('${images}/img_3.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <p class="lead">This is a description for the second slide.</p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('${images}/img_2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <p class="lead">This is a description for the third slide.</p>
        </div>
      </div>
      <!-- Slide Four - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('${images}/img_2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <p class="lead">This is a description for the forth slide.</p>
        </div>
      </div>
      <!-- Slide Four - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('${images}/img_2.jpg')">
        <div class="carousel-caption d-none d-md-block">
          <p class="lead">This is a description for the forth slide.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
</header><br/>





<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <c:forEach var="product" items="${product}">
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card h-100">
                        <a href="#"><img class="card-img-top" src="${contextRoot}/static-resource/images/${product.code}.jpg" alt="Product Image"></a>
                        <div class="card-body">
                            <h4 class="card-title">
                                <a href="#">${product.name}</a>
                            </h4>
                                <h5>&#x9f3; ${product.unitPrice}</h5>
                                <p class="card-text">${product.description}</p>
                        </div>
                        <div class="card-footer">
                            <input type="button" class="btn btn-primary" value="Add to Cart"/>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->