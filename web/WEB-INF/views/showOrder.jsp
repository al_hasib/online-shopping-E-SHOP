<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>
        <script type="text/javascript">


        </script>
        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div style="background-color: #eaeaea">
                <table>
                    <tr>
                        <td><b>Total Price: </b></td>
                        <td>${details.orderTotal}</td>
                    </tr>
                    <tr>
                        <td><b>Total Item: </b></td>
                        <td>${details.orderCount}</td>
                    </tr>
                </table>
            </div>
            <br/>
            <div class="row">                
                <c:forEach items="${order}" var="order">
                    <div class="col-md-4">              
                        <div>
                            <img src="static-resource/images/${order.product.code}.jpg" alt="" style="height: 150px; width: 150px"/><br/>
                        </div>
                        <form>
                            <div>
                                <table style="font-size: 15px;">
                                    <tr class="hidden">
                                        <td><b>Order ID: </b></td>
                                        <td>${order.id}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Product ID: </b></td>
                                        <td>${order.product.id}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Name: </b></td>
                                        <td>${order.product.name}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Brand: </b></td>
                                        <td>${order.product.brand}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Description: </b></td>
                                        <td>${order.product.description}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Buying Price: </b></td>
                                        <td>${order.buyingPrice}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Quantity: </b></td>
                                        <td>${order.productCount}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Buying Price: </b></td>
                                        <td>${order.total}</td>
                                    </tr>
                                    <tr class="hidden">
                                        <td><b>Order Details Id: </b></td>
                                        <td>${order.orderDetail.id}</td>
                                    </tr>
                                </table>
                            </div> 
                        </form>
                    </div>
                </c:forEach>
            </div>
            <br/>
        </div>

    </body>
</html>
