<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            

        </script>
        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div class="row">
                <c:forEach items="${cart}" var="cart">
                    <div class="col-md-4">              
                        <div>
                            <img src="static-resource/images/${cart.product.code}.jpg" alt="" style="height: 150px; width: 150px"/><br/>
                        </div>
                        <form action="/E-Shop/order.idb?cartId=${cart.id}" method="post" name="f">
                            <div>
                                <table style="font-size: 15px;">
                                    <tr class="hidden">
                                        <td>Cart ID</td>
                                        <td><input type="text" value="${cart.id}" style="border: 0" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td>ID</td>
                                        <td><input type="text" value="${cart.product.id}" style="border: 0" name="product.id" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>${cart.product.name}</td>
                                    </tr>
                                    <tr>
                                        <td>Brand</td>
                                        <td>${cart.product.brand}</td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td>${cart.product.description}</td>
                                    </tr>
                                    <tr>
                                        <td>Buying Price</td>
                                        <td><input type="text" value="${cart.product.unitPrice}" style="border: 0" name="buyingPrice" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td>Quantity</td>
                                        <td><input type="text" value="${cart.productCount}" style="border: 0" name="productCount" readonly/></td>
                                    </tr>
                                    <tr>
                                        <td>Total Price</td>
                                        <td><input type="text" value="${cart.product.unitPrice}" style="border: 0" name="total" readonly/></td>
                                    </tr>
                                    <tr class="hidden">
                                        <td>Total Price</td>
                                        <td><input type="text" value="${odId}" style="border: 0" name="orderDetail.id" readonly/></td>
                                    </tr>
                                </table>
                            </div> 
                            <div>
                                <input type="submit" value="Order Now" class="btn btn-primary"/> &nbsp;                                
                            </div>
                        </form>
                    </div>
                    <br/>
                </c:forEach>                
            </div>
            <br/>
            <a class="btn btn-info pull-left" href="/E-Shop/showOrder.idb?odId=${odId}">Show Order</a>
            <br/>
        </div>

    </body>
</html>
