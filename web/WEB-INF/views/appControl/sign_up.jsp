<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f"  uri="http://www.springframework.org/tags/form"%>
<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>

<!--
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         Bootstrap Core CSS 
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>
        <title>Online Shop</title>-->
<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">
<link href="${css}/login.css" rel="stylesheet">
<!-- Bootstrap js -->
<script src="${js}/bootstrap.min.js"></script>
<script src="${js}/login.js"></script>
<!-- jQuery -->
<script src="${js}/jquery.min.js" type="text/javascript"></script>
<!--</head>-->
<!--<body>-->

<%--<%@include file="../shared/navbar.jsp"%>--%>
<div class="login" style="top: 50%;">
    <h2 style="color:#ffffff;padding-left: 60px;">Sign Up</h2>
    <form method="post" action="/E-Shop/signup.idb">
        <!--<label></label>-->
        <input type="text" name="firstName" placeholder="Enter First Name" required><br/>
        <!--<label>Last Name</label>-->
        <input type="text" name="lastName" placeholder="Enter Last Name" required><br/>
        <!--<label>Email Address</label>-->
        <input type="email" name="email" placeholder="Enter E-mail address" required><br/>
        <!--<label>Contact No</label>-->
        <input type="text" name="contactNumber" placeholder="Enter Your Contact no." required><br/>               
        <!--<label>Address One</label>-->
        <input type="text" name="addressLineOne" placeholder="Enter Address One" required><br/>
        <!--<label>Address Two</label>-->
        <input type="text" name="addressLineTwo" placeholder="Enter Address Two" required><br/>
        <!--<label>City</label>-->
        <input type="text" name="city" placeholder="Enter Your City" required><br/>
        <!--<label>State</label>-->
        <input type="text" name="state" placeholder="Enter Your State" required><br/>
        <!--<label>Country</label>-->
        <input type="text" name="country" placeholder="Enter Country Name" required><br/>
        <!--<label>Password</label>-->
        <input type="password" name="password" placeholder="Enter Your Password" required><br/>                
        <input type="submit" value="Sign up" class="btn btn-primary btn-block btn-large">
        <h6 style="color: #ffffff; text-decoration: none">Already have account? <a href="/E-Shop/signin.idb">SIGN IN</a></h6>
    </form>
</div>
<%--<%@include file="../shared/footer.jsp"%>--%>



