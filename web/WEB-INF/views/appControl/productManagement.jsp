<%@taglib  prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>

<c:set var="contextRoot" value="${pageContext.request.contextPath}" /> 



<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Theme -->
<link href="${css}/bootstrap-theme.css" rel="stylesheet" type="text/css"/>

<!-- Custom CSS -->
<link href="${css}/shop-homepage.css" rel="stylesheet">
<link href="${css}/productManagement.css" rel="stylesheet" type="text/css"/>
<%@include file="../shared/navbar.jsp" %>

<script>
    function deleteProduct(id) {
        if (confirm('Do you realy want to delete this Question?')) {
            var url = '${contextRoot}/product/deleteProduct.idb?id=' + id;
            window.location.href = url;
        }
    }
</script>
<div class="container">
    <div class="row">
        <div class="col-md-3" style="margin-top: 30px;border-width: 4px;border: #23272b;">
            <a href="${contextRoot}/product/add.idb" class="anchor">Add Product</a><br>
            <a href="${contextRoot}/product/showAllOrder.idb" class="anchor">Order List</a>

        </div>
        <div class="col-md-9">

            <section>
                <!--for demo wrap-->
                <h1>Product Table</h1>

                <div class="tbl-header">
                    <table cellpadding="0" cellspacing="5" border="0">
                        <thead>
                            <tr>
                                <th>ID </th>
                                <th>Name</th>
                                <th style="padding-left: -5px;">Category</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Image</th>
                                <th>Modify</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <c:forEach var="product" items="${product}">
                    <div class="tbl-content">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr style="padding-bottom: 0px;margin-bottom: 0px;">
                                    <td>${product.id}</td>
                                    <td>${product.name}</td>
                                    <td>${product.categoryId}</td>
                                    <td>${product.unitPrice}</td>
                                    <td>${product.quantity}</td>
                                    <td style="text-align: right; padding-left: 0px;"><img src="${contextRoot}/static-resource/images/${product.code}.jpg" style="width: 80px;height: 80px;text-align: right;"/></td>
                                    <td style="text-align: right; padding-left: 0px;">
                                        <a href="${contextRoot}/product/update.idb?id=${product.id}" class="btn btn-primary csBtn" >Edit</a><br>
                                        <a class="btn btn-danger csBtn" onclick="javascript:deleteProduct(${product.id})">Delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </c:forEach>
            </section>
        </div>
    </div>

</div>


<!-- jQuery -->
<script src="${js}/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap js -->
<script src="${js}/bootstrap.min.js"></script>

<script src="${js}/productManagement.js" type="text/javascript"></script>

<!-- jQuery DataTables Plugin -->
<!--<script src="${js}/dataTables.bootstrap.js"></script>-->

<!-- Self coded javascript -->
<script src="${js}/myapp.js"></script>

<!--<script src="${js}/JQuery-3.3.1.slim.min.js" type="text/javascript"></script>-->
<script src="${js}/bootstrap.bundle.min.js" type="text/javascript"></script>

<%@include file="../shared/footer.jsp" %>