<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f"  uri="http://www.springframework.org/tags/form"%>
<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>

<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">
<link href="${css}/login.css" rel="stylesheet">
<!-- Bootstrap js -->
<script src="${js}/bootstrap.min.js"></script>
<script src="${js}/login.js"></script>
<!-- jQuery -->
<script src="${js}/jquery.min.js" type="text/javascript"></script>


<div class="login">
	<h1>Sign In</h1>
        <form method="post" action="/E-Shop/signin.idb">
    	<input type="text" name="email" placeholder="Email" required="required" />
        <input type="password" name="password" placeholder="Password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large">Let me in.....</button>
        <h6 style="color: #ffffff; text-decoration: none">If No account <a href="/E-Shop/signup.idb">SIGN UP</a></h6>
    </form>
</div>
