<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>

        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-bordred table-striped">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Product Brand</th>
                            <th>Buying Price</th>
                            <th>Total Product</th>
                            <th>Total Price</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${order}" var="order"> 
                            <tr>
                                <td>${order.id}</td>
                                <td>${order.product.id}</td>
                                <td>${order.product.name}</td>
                                <td>${order.product.brand}</td>
                                <td>${order.buyingPrice}</td>
                                <td>${order.productCount}</td>
                                <td>${order.total}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
