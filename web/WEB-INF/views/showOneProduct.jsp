
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags"%>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>

<c:set var="contextRoot" value="${pageContext.request.contextPath}" /> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>
        <title>Online Shop</title>
    </head>
    <body>
        <br/>
        <div class="container">
            <center>
                <div>
                    <img src="static-resource/images/${pro.code}.jpg" alt="" style="height: 200px; width: 200px"/><br/>
                </div>
                <form action="/E-Shop/addCart.idb" method="post">
                    <div>
                        <table>
                            <tr>
                                <td>ID</td>
                                <td><input type="text" value="${pro.id}" style="border: 0" name="product.id" readonly/></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><input type="text" value="${pro.name}" style="border: 0" readonly/></td>
                            </tr>
                            <tr>
                                <td>Brand</td>
                                <td><input type="text" value="${pro.brand}" style="border: 0" readonly/></td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>${pro.description}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td><input type="text" value="${pro.unitPrice}" style="border: 0" name="buyingPrice" readonly/></td>
                            </tr>
                            <tr class="hidden">
                                <td>Price</td>
                                <td><input type="text" value="${pro.unitPrice}" style="border: 0" name="total" readonly/></td>
                            </tr>
                            <tr>
                                <td>Quantity</td>
                                <td><input type="text" value="1" style="border: 0" name="productCount" /></td>
                            </tr>
                        </table>

                    </div> 
                    <div>
                        <input type="submit" value="Add to Cart" /> &nbsp;
                        <input type="button" value="Buy" /><br/>
                    </div>
                </form>
            </center>
        </div>

    </body>
</html>
