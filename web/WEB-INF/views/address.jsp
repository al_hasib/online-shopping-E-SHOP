<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap2.min.css" rel="stylesheet">
        <script src="${js}/jquery2.min.js" type="text/javascript"></script>
        <script src="${js}/bootstrap2.min.js" type="text/javascript"></script>

        <title>Online Shop</title>
    </head>
    <body>

        <br/>
        <div class="container">
            <div class="table-responsive">
                <table class="table table-bordred table-striped">
                    <tr>
                        <td>Name</td>
                        <td>${users.firstName} ${users.lastName}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>${users.email}</td>
                    </tr>
                    <tr>
                        <td>Contact No</td>
                        <td>${users.contactNumber}</td>
                    </tr>
                    <tr>
                        <td>Address One</td>
                        <td>${user.addressLineOne}</td>
                    </tr>
                    <tr>
                        <td>Address Two</td>
                        <td>${user.addressLineTwo}</td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>${user.city}</td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>${user.country}</td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
