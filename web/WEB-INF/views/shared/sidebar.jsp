<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="my-4">Product Category</h3>
<div class="list-group">
        <c:forEach items="${categories}" var="category">
		<a href="${contextRoot}/show/category/${category.id}/products.idb" class="list-group-item thead-light" id="a_${category.name}">${category.name}</a>
	</c:forEach>
</div>                