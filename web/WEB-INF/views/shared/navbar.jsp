<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- Navigation -->
<!--<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <h1><a class="navbar-brand" href="${contextRoot}/index.idb">E-Shop</a></h1>
        <link rel="icon" type="image/ico" href="${images}/logo.png" class="icon"/>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item" id="home">
                    <a class="nav-link" href="${contextRoot}/index.idb">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item" id="listProduct">
                    <a class="nav-link" href="${contextRoot}/show/all/products.idb">View Product</a>
                </li>
                <!--                 <li class="nav-item" id="addProduct">
                                    <a class="nav-link" href="${contextRoot}/product/add.idb"> Add Product </a>
                                </li>-->
                <li class="nav-item" id="about">
                    <a class="nav-link" href="${contextRoot}/about.idb">About Us</a>
                </li>                

                <li class="nav-item" id="contact">
                    <a class="nav-link" href="${contextRoot}/contact.idb">Contact Us</a>
                </li>

                <li class="nav-item" id="sign-in">
                    <a class="nav-link" href="${contextRoot}/showCart.idb">Show Cart</a>
                </li>
                <c:if test="${user == null}">
                    <li class="nav-item" id="sign-in">
                        <a class="nav-link" href="${contextRoot}/signin.idb">Sign In</a>
                    </li>
                </c:if>
                <c:if test="${user != null}">
                    <li class="nav-item" id="sign-in">
                        <a class="nav-link" href="${contextRoot}/signin.idb">${user.name}</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>