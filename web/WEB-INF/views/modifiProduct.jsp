<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<br/><br/>
<div class="container">
    <c:if test="${not empty message}">	
        <div class="row">			
            <div class="col-xs-12 col-md-offset-2 col-md-8">			
                <div class="alert alert-info fade in">${message}</div>				
            </div>
        </div>
    </c:if>
    <div class="row">
        <div class="col-md-offset-2 col-md-8">
            
                <div class="panel-heading">
                    <h4>Product Management</h4>
                </div>
                <f:form class="form-horizontal" modelAttribute="product" action="${contextRoot}/product/add.idb" 
                        method="POST" enctype="multipart/form-data">
                    <f:hidden path="id"  />
                    <div class="form-group">
                        <label class="control-label col-md-4"> Product Name</label>
                        <div class="col-md-8">
                            <f:input type="text" path="name" class="form-control" placeholder="Product Name" />
                            <em class="help-block">Please Enter the Product Name...</em>
                            <!--<sf:errors path="name" cssClass="help-block" element="em"/>--> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Product Brand</label>
                        <div class="col-md-8">
                            <f:input type="text" path="brand" class="form-control" placeholder="Brand Name" /> 
                            <em class="help-block">Please Enter the Product Brand...</em>
                            <!--<sf:errors path="brand" cssClass="help-block" element="em"/>-->	
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Product Category</label>
                        <div class="col-md-8">
                            <f:select path="categoryId" items="${categories}" itemLabel="name" itemValue="id" class="form-control" id="categoryId"/>
                            <em class="help-block">Please Select Category...</em>
                            <div class="text-right">
                                <br/>			
                                <sf:hidden path="id"/>
                                <sf:hidden path="code"/>
                                <sf:hidden path="supplierId"/>
                                <sf:hidden path="active"/>														
                                <sf:hidden path="purchase"/>														
                                <sf:hidden path="view"/>														
                                <!--<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myCategoryModal">Add New Category</button>-->
                            </div>							
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Unit Price</label>
                        <div class="col-md-8">
                            <f:input type="number" path="unitPrice" class="form-control" placeholder="Enter Unit Price" />
                            <em class="help-block">Please Enter Product Price...</em>
                            <!--<sf:errors path="unitPrice" cssClass="help-block" element="em"/>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Product Quantity</label>
                        <div class="col-md-8">
                            <f:input type="number" path="quantity" class="form-control" placeholder="Enter Quantity" />
                            <em class="help-block">Please Enter Product Quantity...</em>
                            <!--<sf:errors path="quantity" cssClass="help-block" element="em"/>--> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="file">Upload a file</label>
                        <div class="col-md-8">
                            <f:input type="file" path="file" id="file" class="form-control"/>
                            <em class="help-block">Please Select an Image...</em>
                            <!--<errors path="file" cssClass="help-block" element="em"/>--> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Product Description</label>
                        <div class="col-md-8">
                            <f:textarea path="description" class="form-control" placeholder="Enter your description here!"/>
                            <em class="help-block">Please Give Some Product Description...</em>
                            <!--<sf:errors path="description" cssClass="help-block" element="em"/>-->
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-offset-4 col-md-4">

                            <input type="submit" name="submit" value="Save" class="btn btn-primary"/>

                        </div>
                    </div>						

                </f:form>



            

        </div>

    </div>

    <!--     Modal 
        <div class="modal fade" id="myCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">New Category</h4>
                    </div>
                    <div class="modal-body">
    
                        <sf:form id="categoryForm" class="form-horizontal" modelAttribute="category" action="${contextRoot}/manage/category" method="POST">
    
                            <div class="form-group">
                                <label class="control-label col-md-4">Name</label>
                                <div class="col-md-8 validate">
                                    <sf:input type="text" path="name" class="form-control"
                                              placeholder="Category Name" /> 
                                </div>
                            </div>
    
                            <div class="form-group">				
                                <label class="control-label col-md-4">Description</label>
                                <div class="col-md-8 validate">
                                    <sf:textarea path="description" class="form-control"
                                                 placeholder="Enter category description here!" /> 
                                </div>
                            </div>	        	        
    
    
                            <div class="form-group">				
                                <div class="col-md-offset-4 col-md-4">					
                                    <input type="submit" name="submit" value="Save" class="btn btn-primary"/>						
                                </div>
                            </div>	        
                        </sf:form>
                    </div>
                </div>
            </div>
        </div>-->
</div>
