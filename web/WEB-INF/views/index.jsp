<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="t" uri="http://www.springframework.org/tags"%>

<c:url var="css" value="/static-resource/css"/>
<c:url var="js" value="/static-resource/js"/>
<c:url var="images" value="/static-resource/images"/>

<c:set var="contextRoot" value="${pageContext.request.contextPath}" /> 
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="HASIB">

        <title>Online Shop - ${title}</title>

        <script>
            window.menu = '${title}';
            window.contextRoot = '${contextRoot}';
        </script>

        <!-- Bootstrap Core CSS -->
        <link href="${css}/bootstrap.min.css" rel="stylesheet">
        
        <!-- Bootstrap Theme -->
        <link href="${css}/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        
        <!--Datatable Bootstrap-->
        <link href="${css}/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>

        <!-- Custom CSS -->
        <link href="${css}/shop-homepage.css" rel="stylesheet">

    </head>

    <body>
        <div class="wrapper col-lg-offset-2">
            <!-- Navigation Bar -->
            <%@include file="shared/navbar.jsp"%>


            <div class="content">
                <!-- Page Contents -->
                <c:if test="${clickHome == true}">
                    <%@include file="./home.jsp" %>
                </c:if> 

                <c:if test="${clickAbout == true}">
                    <%@include file="./about.jsp" %>
                </c:if>

                <c:if test="${clickContact == true}">
                    <%@include file="./contact.jsp" %>
                </c:if>
                
                <c:if test="${clickAllProducts == true or clickCategoryProduct == true}">
                    <%@include file="./listOfAllProducts.jsp" %>
                </c:if>

                <c:if test="${addProducts == true}">
                    <%@include file="./modifiProduct.jsp" %>
                </c:if>
                
                <c:if test="${clickLogin == true}">
                    <%@include file="./appControl/sign_in.jsp"%>
                </c:if>
                <c:if test="${clickSignup == true}">
                    <%@include file="./appControl/sign_up.jsp"%>
                </c:if>
            </div>
            
            
            <!-- Footer -->
            <%@include file="shared/footer.jsp" %>


            <!-- Bootstrap core JavaScript -->
            <!-- jQuery -->
            <script src="${js}/jquery.min.js" type="text/javascript"></script>

            <!-- Bootstrap js -->
            <script src="${js}/bootstrap.min.js"></script>
            
            
            <!-- jQuery DataTables Plugin -->
            <!--<script src="${js}/dataTables.bootstrap.js"></script>-->

            <!-- Self coded javascript -->
            <script src="${js}/myapp.js"></script>
            
            <!--<script src="${js}/JQuery-3.3.1.slim.min.js" type="text/javascript"></script>-->
            <script src="${js}/bootstrap.bundle.min.js" type="text/javascript"></script>
            
            <!-- jQuery DataTables Plugin -->
            <script src="${js}/jquery.dataTables.js"></script>
            
        </div>
    </body>

</html>
