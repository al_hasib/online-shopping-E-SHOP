$(function () {
//   Solving active menu problem
    switch (menu) {
        case 'About Us':
            $('#about').addClass('active');
            break;
        case 'Home':
            $('#home').addClass('active');
            break;
        case 'Contact Us':
            $('#contact').addClass('active');
            break;
        case 'All Product':
            $('#listProduct').addClass('active');
            $('#a_' + menu).addClass('active');
            break;
        case 'Sign in':
            $('#sign-in').addClass('active');
            break;
        case 'Add Product':
            $('#addProduct').addClass('active');
            $('#a_' + menu).addClass('active');
            break;
        default :
            if (menu === "Home")
                break;
            $('#home').addClass('active');
            $('#a_' + menu).addClass('active');
            break;
    }
    var $table = $('#productListTable');
    if ($table.length) {
        var jsonURL = '';
        if (window.categoryId === '') {
            jsonURL = window.contextRoot + '/json/data/all/products.idb';
        } else {
            jsonURL = window.contextRoot + '/json/data/category/' + window.categoryId + '/products.idb';
        }

        $table.DataTable({
            lengthMenu: [[10, 20, 30, -1], ['10 Records', '20 Records', '30 Records', 'All Records']],
            pageLength: 10,
            ajax: {
                url: jsonURL,
                dataSrc: ''
            },
            columns: [
                {data: 'code',
                    bSortable: false,
                    mRender: function (data, type, row) {
                        return '<img src="' + window.contextRoot+ '/static-resource/images/' 
                                + data + '.jpg" class="dataTableImg"/>';
                    }
                },
                {data: 'name'},
                {data: 'brand'},
                {data: 'unitPrice',
                    mRender: function (data, type, row) {
                        return '&#2547 ' + data;
                    }
                },
                {data: 'quantity',
                    mRender: function (data, type, row) {
                        if (data < 1) {
                            return '<span style="color:red">Out of Stock!</span>';
                        }
                        return data;
                    }
                },
                {data: 'id',
                    bSortable: false,
                    mRender: function (data, type, row) {
                        var str = '';
                        str += '<a href="' + window.contextRoot + '/showProduct.idb?id='+ data + '" class="btn btn-success">'
                                +'Show</a>';
//                        str += '<a href="'+ window.contextRoot+'/product/update.idb?id='+
//                                data + '" class="btn btn-primary">Add</a>';
                        return str;
                    }

                }
//                {data: 'id',
//                    bSortable: false,
//                    mRender: function (data, type, row) {
//                        var str = '';
//                        str += '<a href="' + window.contextRoot + '/showProduct.idb?id='+ data + '" class="btn btn-success">'
//                                +'Add to Cart/Buy</a>';
//                        str += '<a href="'+ window.contextRoot+'/product/update.idb?id='+
//                                data + '" class="btn btn-primary">view</a>';
//                        return str;
//                    }
//
//                }
            ]
        });
    }
});

