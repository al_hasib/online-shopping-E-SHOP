package com.idb.dao;

import com.idb.dto.Cart;
import com.idb.dto.CartLine;
import com.idb.dto.OrderDetail;
import com.idb.dto.OrderItem;
import java.util.List;
import java.sql.Date;

public interface CartLineDAO {

	public List<CartLine> list(int uid);
	public CartLine get(int id);	
	public boolean add(CartLine cartLine);
	public boolean update(CartLine cartLine);
	public boolean remove(int id);
        public boolean addOrder(OrderItem orderItem);
        
	// fetch the CartLine based on cartId and productId
	public CartLine getByCartAndProduct(int cartId, int productId);		
		
	// updating the cart
	boolean updateCart(Cart cart);
	
	// list of available cartLine
	public List<CartLine> listAvailable(int cartId);
        public OrderDetail listOdById(int id);
	public boolean updateOrderDetail(OrderDetail orderDetail);
	// adding order details
	boolean addOrderDetail(OrderDetail orderDetail);
        
        int getByUserId(int userId, Date date);
        public List<OrderItem> listOrder(int oDetailsId);
        
        List<OrderDetail> listOrderDetail();

	
}
