package com.idb.dao;

import com.idb.dto.Category;
import java.util.List;

public interface CategoryDAO {
    boolean add(Category category);
    List<Category> catList();
    Category get(int id);
    boolean updateCategory(Category category);
    boolean deleteCategory(Category category);
}
