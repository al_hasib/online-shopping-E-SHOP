package com.idb.dao;

import com.idb.dto.Address;
import com.idb.dto.CartLine;
import com.idb.dto.User;
import java.util.List;

public interface UserDAO {

	// user related operation
	User getByEmail(String email);
	User get(int id);
        boolean getByProductID(int id);
	boolean add(User user);
	String login(String email, String pass);
	// adding and updating a new address
	Address getAddress(int addressId);
	boolean addAddress(Address address);
	boolean updateAddress(Address address);
	Address getBillingAddress(int userId);
	List<Address> listShippingAddresses(int userId);
        Address listAddress(int userId);

}
