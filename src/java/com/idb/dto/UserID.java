
package com.idb.dto;

public class UserID {
    
    private static String email;
    private static int uID;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getuID() {
        return uID;
    }

    public void setuID(int uID) {
        UserID.uID = uID;
    }
    
    
    
    
}
