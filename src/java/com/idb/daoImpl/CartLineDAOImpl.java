package com.idb.daoImpl;

import com.idb.dao.CartLineDAO;
import com.idb.dto.Cart;
import com.idb.dto.CartLine;
import com.idb.dto.OrderDetail;
import com.idb.dto.OrderItem;
import java.sql.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("cartLineDAO")
@Transactional
public class CartLineDAOImpl implements CartLineDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public CartLine getByCartAndProduct(int cartId, int productId) {
        String query = "FROM CartLine WHERE cartId = :cartId AND product.id = :productId";
        try {

            return sessionFactory.getCurrentSession()
                    .createQuery(query, CartLine.class)
                    .setParameter("cartId", cartId)
                    .setParameter("productId", productId)
                    .getSingleResult();

        } catch (Exception ex) {
            return null;
        }

    }

    @Override
    public boolean add(CartLine cartLine) {
        try {
            sessionFactory.getCurrentSession().persist(cartLine);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(CartLine cartLine) {
        try {
            sessionFactory.getCurrentSession().update(cartLine);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean remove(int id) {
        try {
            CartLine cartLine = get(id);
            sessionFactory.getCurrentSession().delete(cartLine);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public List<CartLine> list(int uId) {
        String query = "FROM CartLine WHERE userId = :userId";
        return sessionFactory.getCurrentSession().createQuery(query, CartLine.class)
                .setParameter("userId", uId)
                .getResultList();
    }

    @Override
    public CartLine get(int id) {
        return sessionFactory.getCurrentSession().get(CartLine.class, Integer.valueOf(id));
    }

    @Override
    public boolean updateCart(Cart cart) {
        try {
            sessionFactory.getCurrentSession().update(cart);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public List<CartLine> listAvailable(int cartId) {
        String query = "FROM CartLine WHERE cartId = :cartId AND available = :available";
        return sessionFactory.getCurrentSession()
                .createQuery(query, CartLine.class)
                .setParameter("cartId", cartId)
                .setParameter("available", true)
                .getResultList();
    }

    @Override
    public boolean addOrderDetail(OrderDetail orderDetail) {
        try {
            sessionFactory.getCurrentSession().persist(orderDetail);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public int getByUserId(int userId, Date date) {
        int id = 0;
        String selectQuery = "FROM OrderDetail WHERE userId = :userId AND orderDate = :date ";
        try {
            List<OrderDetail> list = sessionFactory.getCurrentSession()
                    .createQuery(selectQuery, OrderDetail.class)
                    .setParameter("userId", userId)
                    .setParameter("date", date)
                    .list();
            if ((list != null) && (list.size() == 1)) {
                for (OrderDetail b : list) {
                    id = b.getId();
//                    System.out.println(role);
                }

            }
        } catch (Exception ex) {
        }
        return id;
    }
    

    @Override
    public boolean addOrder(OrderItem orderItem) {
        try {
            sessionFactory.getCurrentSession().persist(orderItem);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    

    @Override
    public OrderDetail listOdById(int id) {
        try {
            return sessionFactory.getCurrentSession().get(OrderDetail.class, Integer.valueOf(id));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateOrderDetail(OrderDetail orderDetail) {
        try {
            sessionFactory.getCurrentSession().update(orderDetail);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public List<OrderItem> listOrder(int oDetailsId) {
        String query = "FROM OrderItem WHERE orderDetail.id = :id";
        return sessionFactory.getCurrentSession().createQuery(query, OrderItem.class)
                .setParameter("id", oDetailsId)
                .getResultList();
    }

    @Override
    public List<OrderDetail> listOrderDetail() {
        return sessionFactory.getCurrentSession().createQuery("FROM OrderDetail", OrderDetail.class).getResultList();
    }

}
