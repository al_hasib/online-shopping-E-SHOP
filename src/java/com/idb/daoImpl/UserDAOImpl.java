package com.idb.daoImpl;

import com.idb.dao.UserDAO;
import com.idb.dto.Address;
import com.idb.dto.CartLine;
import com.idb.dto.User;
import com.idb.dto.UserID;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("userDAO")
@Transactional
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public User getByEmail(String email) {
        String selectQuery = "FROM User WHERE email = :email";
        try {
            return sessionFactory
                    .getCurrentSession()
                    .createQuery(selectQuery, User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }

    }

    @Override
    public boolean add(User user) {
        try {
            sessionFactory.getCurrentSession().persist(user);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean addAddress(Address address) {
        try {
            // will look for this code later and why we need to change it
            sessionFactory.getCurrentSession().persist(address);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean updateAddress(Address address) {
        try {
            sessionFactory.getCurrentSession().update(address);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    
    @Override
    public Address listAddress(int userId) {
        String query = "FROM Address WHERE userId = :id";
        return sessionFactory.getCurrentSession().createQuery(query, Address.class)
                .setParameter("id", userId)
                .getSingleResult();
    }

    @Override
    public List<Address> listShippingAddresses(int userId) {
        String selectQuery = "FROM Address WHERE userId = :userId AND shipping = :isShipping ORDER BY id DESC";
        return sessionFactory
                .getCurrentSession()
                .createQuery(selectQuery, Address.class)
                .setParameter("userId", userId)
                .setParameter("isShipping", true)
                .getResultList();

    }

    @Override
    public Address getBillingAddress(int userId) {
        String selectQuery = "FROM Address WHERE userId = :userId AND billing = :isBilling";
        try {
            return sessionFactory
                    .getCurrentSession()
                    .createQuery(selectQuery, Address.class)
                    .setParameter("userId", userId)
                    .setParameter("isBilling", true)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public User get(int id) {
        try {
            return sessionFactory.getCurrentSession().get(User.class, id);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Address getAddress(int addressId) {
        try {
            return sessionFactory.getCurrentSession().get(Address.class, addressId);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public String login(String email, String pass) {
        String role = "";
        String selectQuery = "FROM User WHERE email = :email AND password = :password ";
        try {
            List<User> list = sessionFactory.getCurrentSession().createQuery(selectQuery, User.class).setParameter("email", email)
                    .setParameter("password", pass).list();
            if ((list != null) && (list.size() == 1)) {

                for (User b : list) {
                    role = b.getRole();
//                    System.out.println(role);
                }

            }
        } catch (Exception ex) {
        }
        return role;
    }

    @Override
    public boolean getByProductID(int id) {
        UserID userID = new UserID();
        String selectQuery = "FROM CartLine WHERE product.id = :id AND emailId = :email";
        try {
            List<CartLine> list = sessionFactory
                    .getCurrentSession()
                    .createQuery(selectQuery, CartLine.class)
                    .setParameter("id", id)
                    .setParameter("email", userID.getEmail())
                    .list();
            if (list.size() == 1) {
                return true;
            }
        } catch (Exception ex) {
            return false;
        }
        return false;
    }

}
