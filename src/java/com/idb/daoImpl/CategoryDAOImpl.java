package com.idb.daoImpl;

import com.idb.dao.CategoryDAO;
import com.idb.dto.Category;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("categoryDAO")
@Transactional
public class CategoryDAOImpl implements CategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /*
	 * Getting single category based on id
     */
    @Override
    @SuppressWarnings("unchecked")
    public Category get(int id) {
        return sessionFactory.getCurrentSession().get(Category.class, Integer.valueOf(id));
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean add(Category category) {
        try {
            // add the category to the database table
            sessionFactory.getCurrentSession().persist(category);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Category> catList() {
        String selectActiveCategory = "FROM Category WHERE active = :active";
        Query query = sessionFactory.getCurrentSession().createQuery(selectActiveCategory);
        query.setParameter("active", true);
        return query.getResultList();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public boolean updateCategory(Category category) {
        try {
            // add the category to the database table
            sessionFactory.getCurrentSession().update(category);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean deleteCategory(Category category) {
        category.setActive(false);
        try {
            // add the category to the database table
            sessionFactory.getCurrentSession().update(category);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
