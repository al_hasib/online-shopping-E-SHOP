package com.idb.controllers;

import com.idb.config.FileUploadUtility;
import com.idb.dao.CartLineDAO;
import com.idb.dao.CategoryDAO;
import com.idb.dao.ProductDAO;
import com.idb.dao.UserDAO;
import com.idb.dto.Category;
import com.idb.dto.Product;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ProductDAO productDAO;
    
    @Autowired
    private CartLineDAO cartLineDAO;
    
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/product/add.idb")
    public ModelAndView showAddProducts(@RequestParam(name = "status", required = false) String status) {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("addProducts", true);
        mv.addObject("title", "Add Product");
        Product newProduct = new Product();
        if (status != null) {
            if (status.equals("product")) {
                mv.addObject("message", "Product Submission Successful !!!");
            }
        }
        mv.addObject("product", newProduct);
        return mv;
    }

    @RequestMapping(value = "/product/add.idb", method = RequestMethod.POST)
    public String addProducts(@ModelAttribute("product") Product mProduct, HttpServletRequest request) {
        if (!mProduct.getFile().getOriginalFilename().equals("")) {
            FileUploadUtility.uploadFile(request, mProduct.getFile(), mProduct.getCode());
        }

        if (mProduct.getId() == 0) {
            mProduct.setSupplierId(1);
            mProduct.setActive(true);
            productDAO.add(mProduct);
        } else {
            mProduct.setSupplierId(1);
            mProduct.setActive(true);
            productDAO.update(mProduct);
        }

        return "redirect:/product/manageProduct.idb";
    }

    @ModelAttribute("categories")
    public List<Category> getCategory() {
        return categoryDAO.catList();
    }

    @RequestMapping(value = "/product/update.idb")
    public ModelAndView showUpdateProducts(@RequestParam(name = "status", required = false) String status, @RequestParam("") Integer id) {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("addProducts", true);
        mv.addObject("title", "Edit Product");
        Product product = productDAO.get(id);
        Product newProduct = product;
        if (status != null) {
            if (status.equals("product")) {
                mv.addObject("message", "Product Submission Successful !!!");
            }
        }
        mv.addObject("product", newProduct);
        return mv;
    }

//    @RequestMapping(value = "/show/product/{id}/update.idb", method = RequestMethod.POST)
//    public String pdateProducts(@ModelAttribute("product") Product mProduct, HttpServletRequest request,@PathVariable("id")int id) {
//        if (!mProduct.getFile().getOriginalFilename().equals("")) {
//            FileUploadUtility.uploadFile(request, mProduct.getFile(), mProduct.getCode());
//        }
//
//        mProduct.setSupplierId(1);
//        mProduct.setActive(true);
//        productDAO.add(mProduct);
//        return "redirect:/show/all/products.idb";
//    }
    
    @RequestMapping(value = "/product/manageProduct.idb")
    public ModelAndView productsManage(@RequestParam(name = "status", required = false) String status) {
        ModelAndView mv = new ModelAndView("appControl/productManagement");
        mv.addObject("addProducts", true);
        mv.addObject("title", "Add Product");
        Product newProduct = new Product();
//        if (status != null) {
//            if (status.equals("product")) {
//                mv.addObject("message", "Product Submission Successful !!!");
//            }
//        }
//        mv.addObject("product", newProduct);
        mv.addObject("category", categoryDAO.catList());
        mv.addObject("product", productDAO.listActiveProducts());
        return mv;
    }
    
    
    @RequestMapping(value = "product/deleteProduct")
    public ModelAndView productDelete(@RequestParam("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:manageProduct.idb");
        productDAO.delete(id);
        return mav;
    }
    
    @RequestMapping(value = "/product/showAllOrder.idb")
    public ModelAndView showAllOrder() {
        ModelAndView mv = new ModelAndView("showAllOrder");
        mv.addObject("order", cartLineDAO.listOrderDetail());
        return mv;
    }
    
    @RequestMapping(value = "/product/showOrderItem.idb")
    public ModelAndView showOrderItem(@RequestParam("id") Integer id) {
        ModelAndView mv = new ModelAndView("showOrderItem");
        mv.addObject("order", cartLineDAO.listOrder(id));
        return mv;
    }
    
    @RequestMapping(value = "/product/showUser.idb")
    public ModelAndView showUser(@RequestParam("id") Integer id) {
        ModelAndView mv = new ModelAndView("address");
        mv.addObject("user", userDAO.listAddress(id));
        mv.addObject("users", userDAO.get(id));
        return mv;
    }
    
    
}
