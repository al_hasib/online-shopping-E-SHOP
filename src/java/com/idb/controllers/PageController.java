package com.idb.controllers;

import com.idb.dao.CategoryDAO;
import com.idb.dao.ProductDAO;
import com.idb.dto.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {
    
    @Autowired
    private CategoryDAO categoryDAO;
    
    @Autowired
    private ProductDAO productDAO;

    public CategoryDAO getCategoryDAO() {
        return categoryDAO;
    }
    
    @RequestMapping(value = {"/", "/index"})
    public ModelAndView showHome() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "Home");
        mv.addObject("product", productDAO.listActiveProducts());
        mv.addObject("categories", categoryDAO.catList());
        mv.addObject("clickHome", true);
        return mv;
    }
    
    @RequestMapping(value = "/about.idb")
    public ModelAndView about() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "About Us");
        mv.addObject("clickAbout", true);
        return mv;
    }
    
    @RequestMapping(value = {"/contact.idb"})
    public ModelAndView contact() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "Contact Us");
        mv.addObject("clickContact", true);
        return mv;
    }
    
//    Load All Product Based on Cateory
    @RequestMapping(value = "/show/all/products.idb")
    public ModelAndView listProduct() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "All Product");
        mv.addObject("categories", categoryDAO.catList());
        mv.addObject("product", productDAO.listActiveProducts());
        mv.addObject("clickAllProducts", true);
        return mv;
    }
    
    @RequestMapping(value = "/show/category/{id}/products.idb")
    public ModelAndView listCategoriseProduct(@PathVariable("id") int id) {
        ModelAndView mv = new ModelAndView("index");
        
//        Category to fatch a single category
        Category category = null;
        category = categoryDAO.get(id);
        
        mv.addObject("categories", categoryDAO.catList());
//        mv.addObject("product", productDAO.listActiveProductsByCategory(id));
        mv.addObject("title", category.getName());
        mv.addObject("category", category);
        mv.addObject("clickCategoryProduct", true);
        return mv;
    }
    
    
    
    @RequestMapping(value = "/show/product/{id}/details.idb")
    public ModelAndView showSingleProduct(@PathVariable("id") int id) {
        ModelAndView mv = new ModelAndView("index");
        
//        Category to fatch a single category
        Category category = null;
        category = categoryDAO.get(id);
        
        mv.addObject("categories", categoryDAO.catList());
        mv.addObject("product", productDAO.listActiveProducts());
        mv.addObject("title", category.getName());
        mv.addObject("category", category);
        mv.addObject("productDetails", true);
        return mv;
    }
}
