package com.idb.controllers;

import com.idb.dao.ProductDAO;
import com.idb.dto.Product;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/json/data")
public class JsonDataController {
    
    @Autowired
    private ProductDAO productDAO;
    
    @RequestMapping("/all/products.idb")
    @ResponseBody
    public List<Product> getAllProducts(){
        return productDAO.listActiveProducts();
    }
    
    @RequestMapping("/category/{id}/products.idb")
    @ResponseBody
    public List<Product> getProductsByCategory(@PathVariable int id){
        return productDAO.listActiveProductsByCategory(id);
    }
    
    @RequestMapping("/product/{id}/details.idb")
    @ResponseBody
    public List<Product> showSingleProduct(@PathVariable int id){
        return productDAO.listActiveProductsByCategory(id);
    }
    
//    @RequestMapping("/product/{id}/update.idb")
//    @ResponseBody
//    public List<Product> updateSingleProduct(@PathVariable int id){
//        return productDAO.listActiveProductsByCategory(id);
//    }
//    
    
}
