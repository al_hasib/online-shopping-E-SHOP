package com.idb.controllers;

import com.idb.dao.CartLineDAO;
import com.idb.dao.ProductDAO;
import com.idb.dao.UserDAO;
import com.idb.dto.Address;
import com.idb.dto.CartLine;
import com.idb.dto.OrderDetail;
import com.idb.dto.OrderItem;
import com.idb.dto.User;
import com.idb.dto.UserID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private CartLineDAO cartLineDAO;

    @Autowired
    private ProductDAO productDAO;

    @RequestMapping(value = "/signin.idb")
    public ModelAndView signIn() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "Sign in");
        mv.addObject("clickLogin", true);
        return mv;
    }

    @RequestMapping(value = "/signup.idb")
    public ModelAndView signUp() {
        ModelAndView mv = new ModelAndView("appControl/sign_up");
//        mv.addObject("title", "Sign up");
//        mv.addObject("clickLogin", true);
        return mv;
    }

    @RequestMapping(value = "/signup.idb", method = RequestMethod.POST)
    public String saveUser(ModelMap map, @ModelAttribute("user") User user, @ModelAttribute("address") Address address) {
        user.setRole("USER");
        userDAO.add(user);
        User auser = userDAO.getByEmail(user.getEmail());
        address.setUserId(auser.getId());
        userDAO.addAddress(address);
        ModelAndView mv = new ModelAndView();
        mv.addObject("clickSignup", true);
        mv.addObject("user", user);
        return "redirect:/signin.idb";
    }

    @RequestMapping(value = "/signin.idb", method = RequestMethod.POST)
    public String checkAdmin(ModelMap map, @ModelAttribute("user") User user) {
        String userExist = userDAO.login(user.getEmail(), user.getPassword());
        ModelAndView mv = new ModelAndView();
        mv.addObject("clickLogin", true);
        if (userExist.equals("ADMIN")) {
            map.put("email", user.getEmail());
            return "redirect:/product/manageProduct.idb";
        } else if (userExist.equals("USER")) {
            User u = userDAO.getByEmail(user.getEmail());
            UserID userId = new UserID();
            userId.setEmail(user.getEmail());
            userId.setuID(u.getId());
//            System.out.println(userId.getEmail() + "----------------------");
            map.put("email", user.getEmail());
            return "redirect:/show/all/products.idb";
        }
        return "redirect:/signin.idb";
    }

    @RequestMapping(value = "/showCart.idb")
    public ModelAndView showCart() {
        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            ModelAndView mv = new ModelAndView("redirect:signin.idb");
            return mv;
        } else {
            ModelAndView mv = new ModelAndView("showCart");
            mv.addObject("cart", cartLineDAO.list(id));
            return mv;
        }
    }

    @RequestMapping(value = "/checkout.idb")
    public ModelAndView checkout() {
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            ModelAndView mv = new ModelAndView("redirect:signin.idb");
            return mv;
        } else {
            int orderId = cartLineDAO.getByUserId(id, date);
            if (orderId == 0) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setUserId(id);
                orderDetail.setOrderDate(date);
                cartLineDAO.addOrderDetail(orderDetail);
            }
            orderId = cartLineDAO.getByUserId(id, date);
            ModelAndView mv = new ModelAndView("checkOut");
            mv.addObject("cart", cartLineDAO.list(id));
            mv.addObject("odId", orderId);
            return mv;

        }
    }

    @RequestMapping(value = "/addCart.idb", method = RequestMethod.POST)
    public String addCart(ModelMap map, @ModelAttribute("cartLine") CartLine cartLine) {

        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            System.out.println(email);
            map.put("msg", "Please sign in if you want to add cart");
            return "redirect:/signin.idb";
        } else if (userDAO.getByProductID(cartLine.getProduct().getId())) {
            return "redirect:/show/all/products.idb";
        } else {
            cartLine.setEmailId(userId.getEmail());
            cartLine.setUserId(id);
            boolean add = cartLineDAO.add(cartLine);
            return "redirect:/showCart.idb";
        }

    }

    @RequestMapping(value = "/editCart.idb", method = RequestMethod.POST)
    public String editCart(ModelMap map, @ModelAttribute("cartLine") CartLine cartLine) {

        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            System.out.println(email);
            map.put("msg", "Please sign in if you want to add cart");
            return "redirect:/signin.idb";
        } else {
            cartLine.setEmailId(userId.getEmail());
            cartLine.setUserId(id);
            boolean add = cartLineDAO.update(cartLine);
            return "redirect:/showCart.idb";
        }

    }

    @RequestMapping(value = "/showProduct.idb")
    public ModelAndView getProduct(@RequestParam("id") Integer id) {
        ModelAndView mv = new ModelAndView("showOneProduct");
        mv.addObject("pro", productDAO.get(id));
        return mv;
    }

    @RequestMapping(value = "deleteCart")
    public ModelAndView cartDelete(@RequestParam("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:showCart.idb");
        cartLineDAO.remove(id);
        return mav;
    }
    
    
    @RequestMapping(value = "/order.idb", method = RequestMethod.POST)
    public String addCOrder(ModelMap map, @ModelAttribute("orderItem") OrderItem orderItem, @RequestParam("cartId") Integer cartId) {

        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            System.out.println(email);
            map.put("msg", "Please sign in if you want to add cart");
            return "redirect:/signin.idb";
        } else {            
            boolean add = cartLineDAO.addOrder(orderItem);
            
            int count = orderItem.getProductCount();
            double price = orderItem.getBuyingPrice();
            double total = count * price;
            
            OrderDetail orderDetail = cartLineDAO.listOdById(orderItem.getOrderDetail().getId());
            int orderCount = orderDetail.getOrderCount();
            double orderTotal = orderDetail.getOrderTotal();
            
            orderDetail.setOrderCount(count + orderCount);
            orderDetail.setOrderTotal(total + orderTotal);
            cartLineDAO.updateOrderDetail(orderDetail);
            
            cartLineDAO.remove(cartId);
            return "redirect:/checkout.idb";
        }

    }
    
    
    @RequestMapping(value = "/showOrder.idb")
    public ModelAndView showOrder(@RequestParam("odId") Integer odId) {
        UserID userId = new UserID();
        String email = userId.getEmail();
        int id = userId.getuID();
        if (email == null) {
            ModelAndView mv = new ModelAndView("redirect:signin.idb");
            return mv;
        } else {
            ModelAndView mv = new ModelAndView("showOrder");
            mv.addObject("order", cartLineDAO.listOrder(odId));
            mv.addObject("details", cartLineDAO.listOdById(odId));
            return mv;
        }
    }
    
}
