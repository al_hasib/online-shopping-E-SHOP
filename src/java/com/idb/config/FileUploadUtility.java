package com.idb.config;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

public class FileUploadUtility {

    private static final String ABS_PATH = "D:\\Chisty\\idb\\JAVA\\Java\\Practical\\NetBeansProjects\\Spring2\\E-Shop-old\\web\\static-resource\\images\\";
    private static String REL_PATH = "";

    public static void uploadFile(HttpServletRequest request, MultipartFile file, String code) {

        REL_PATH = request.getSession().getServletContext().getRealPath("/static-resource/images/");

        if (!new File(ABS_PATH).exists()) {
            new File(ABS_PATH).mkdirs();
        }
        if (!new File(REL_PATH).exists()) {
            new File(REL_PATH).mkdirs();
        }

        try {
            file.transferTo(new File(ABS_PATH + code + ".jpg"));
            file.transferTo(new File(REL_PATH + code + ".jpg"));
        } catch (IOException ie) {

        }
    }
}
