-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.22-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema onlineshopping
--

CREATE DATABASE IF NOT EXISTS onlineshopping;
USE onlineshopping;

--
-- Definition of table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `address_line_one` varchar(100) default NULL,
  `address_line_two` varchar(100) default NULL,
  `city` varchar(20) default NULL,
  `state` varchar(20) default NULL,
  `country` varchar(20) default NULL,
  `postal_code` varchar(10) default NULL,
  `is_billing` tinyint(1) default NULL,
  `is_shipping` tinyint(1) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_address_user_id` (`user_id`),
  CONSTRAINT `fk_address_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`id`,`user_id`,`address_line_one`,`address_line_two`,`city`,`state`,`country`,`postal_code`,`is_billing`,`is_shipping`) VALUES 
 (1,3,'Shekhertek','Dhanmondi','Dhaka','Dhaka',NULL,NULL,0,0),
 (2,4,'Shekhertek','Dhanmondi','Dhaka','Dhaka','sdjkhfk',NULL,0,0),
 (3,2,'House No-13, Road-8, Khilga','Shaymoli','Dhaka','Dhaka','Bangladesh',NULL,0,0);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


--
-- Definition of table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `grand_total` double default NULL,
  `cart_lines` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_cart_user_id` (`user_id`),
  CONSTRAINT `fk_cart_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` (`id`,`user_id`,`grand_total`,`cart_lines`) VALUES 
 (1,2,0,0);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;


--
-- Definition of table `cart_line`
--

DROP TABLE IF EXISTS `cart_line`;
CREATE TABLE `cart_line` (
  `id` int(11) NOT NULL auto_increment,
  `cart_id` int(11) default NULL,
  `total` double default NULL,
  `product_id` int(11) default NULL,
  `product_count` int(11) default NULL,
  `buying_price` double default NULL,
  `is_available` tinyint(1) default NULL,
  `email_id` varchar(45) default NULL,
  `user_id` int(10) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_cartline_product_id` (`product_id`),
  CONSTRAINT `fk_cartline_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_line`
--

/*!40000 ALTER TABLE `cart_line` DISABLE KEYS */;
INSERT INTO `cart_line` (`id`,`cart_id`,`total`,`product_id`,`product_count`,`buying_price`,`is_available`,`email_id`,`user_id`) VALUES 
 (2,0,38000,1,3,19000,1,'jakir@gmail.com',3),
 (5,0,32000,2,1,32000,1,'hasib@gmail.com',2);
/*!40000 ALTER TABLE `cart_line` ENABLE KEYS */;


--
-- Definition of table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image_URL` varchar(255) default NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`,`name`,`description`,`image_URL`,`is_active`) VALUES 
 (1,'Laptop','This is description for Laptop category!','CAT_1.png',1),
 (2,'Television','This is description for Television category!','CAT_2.png',1),
 (3,'Mobile','This is description for Mobile category!','CAT_3.png',1),
 (4,'Micro-wave-oven','This Description for Micro-wave-oven','CAT_4.jpg',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;


--
-- Definition of table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `order_total` double default NULL,
  `order_count` int(11) default NULL,
  `shipping_id` int(11) default NULL,
  `billing_id` int(11) default NULL,
  `order_date` date default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_order_detail_user_id` (`user_id`),
  CONSTRAINT `fk_order_detail_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_detail`
--

/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
INSERT INTO `order_detail` (`id`,`user_id`,`order_total`,`order_count`,`shipping_id`,`billing_id`,`order_date`) VALUES 
 (1,2,115000,3,0,0,'2018-12-02');
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;


--
-- Definition of table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(11) default NULL,
  `total` double default NULL,
  `product_id` int(11) default NULL,
  `product_count` int(11) default NULL,
  `buying_price` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `fk_order_item_product_id` (`product_id`),
  KEY `fk_order_item_order_id` (`order_id`),
  CONSTRAINT `fk_order_item_order_id` FOREIGN KEY (`order_id`) REFERENCES `order_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_order_item_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` (`id`,`order_id`,`total`,`product_id`,`product_count`,`buying_price`) VALUES 
 (1,1,19000,1,1,19000),
 (2,1,48000,5,2,48000);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;


--
-- Definition of table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(20) default NULL,
  `name` varchar(50) default NULL,
  `brand` varchar(50) default NULL,
  `description` varchar(255) default NULL,
  `unit_price` double default NULL,
  `quantity` int(11) default NULL,
  `is_active` tinyint(1) default NULL,
  `category_id` int(11) default NULL,
  `supplier_id` int(11) default NULL,
  `purchases` int(11) default '0',
  `views` int(11) default '0',
  PRIMARY KEY  (`id`),
  KEY `fk_product_category_id` (`category_id`),
  KEY `fk_product_supplier_id` (`supplier_id`),
  CONSTRAINT `fk_product_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_supplier_id` FOREIGN KEY (`supplier_id`) REFERENCES `user_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`,`code`,`name`,`brand`,`description`,`unit_price`,`quantity`,`is_active`,`category_id`,`supplier_id`,`purchases`,`views`) VALUES 
 (1,'PRDABC123DEFX','iPhone 5s','apple','This is one of the best phone available in the market right now!',19000,5,1,3,1,0,0),
 (2,'PRDDEF123DEFX','Samsung s7','samsung','A smart phone by samsung!',32000,2,1,3,1,0,0),
 (3,'PRDPQR123WGTX','Google Pixel','google','This is one of the best android smart phone available in the market right now!',57000,5,1,3,1,0,0),
 (4,'PRDMNO123PQRX',' Macbook Pro','apple','This is one of the best laptops available in the market right now!',54000,3,1,1,1,0,0),
 (5,'PRDABCXYZDEFX','Dell Latitude E6510','dell','This is one of the best laptop series from dell that can be used!',48000,5,1,1,1,0,0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;


--
-- Definition of table `user_detail`
--

DROP TABLE IF EXISTS `user_detail`;
CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL auto_increment,
  `first_name` varchar(50) default NULL,
  `last_name` varchar(50) default NULL,
  `role` varchar(50) default NULL,
  `enabled` tinyint(1) default NULL,
  `password` varchar(50) default NULL,
  `email` varchar(100) default NULL,
  `contact_number` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

/*!40000 ALTER TABLE `user_detail` DISABLE KEYS */;
INSERT INTO `user_detail` (`id`,`first_name`,`last_name`,`role`,`enabled`,`password`,`email`,`contact_number`) VALUES 
 (1,'admin','admin','ADMIN',1,'123','admin@gmail.com','8888888888'),
 (2,'AL','Hasib','USER',1,'123','hasib@gmail.com','7777777777'),
 (3,'Jakir','Hossain','USER',1,'123','jakir@gmail.com','01718987895'),
 (4,'drfrgrdfs','dfgdfg','USER',1,'123','sssarker07@gmail.com','01718987895');
/*!40000 ALTER TABLE `user_detail` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
